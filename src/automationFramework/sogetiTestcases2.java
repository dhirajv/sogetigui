package automationFramework;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.openqa.selenium.By;  
import org.openqa.selenium.WebDriver;  
import org.openqa.selenium.WebElement; 
import org.openqa.selenium.chrome.ChromeDriver;  
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.*;
import java.util.concurrent.TimeUnit;
import java.util.Iterator;
import java.net.MalformedURLException;	
import java.util.List;
import org.openqa.selenium.interactions.Actions;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.IOException;



  


public class sogetiTestcases2 {
	
	//public static void main(String[] args) {
		// TODO Auto-generated method stub

	//	System.setProperty("webdriver.gecko.driver", "C:/Program Files (x86)/Mozilla Firefox/firefox.exe");
	//	WebDriver FirefoxDriver = new FirefoxDriver();
	//	FirefoxDriver.get("http://thedemosite.co.uk/login.php");
					
	//	firstTestcase();
	//	secondTestcase();
	//		thirdTestcase();
				//}

	
	
	public WebDriver driver;
	public String driverPath = "C:/Workspace/chromedriver.exe";
	
	@BeforeTest
	public void single_run(){
		System.setProperty("webdriver.chrome.driver", driverPath);
		driver = new ChromeDriver();
		driver.get("https://www.sogeti.com/");
		driver.manage().window().maximize();
	}
	@AfterTest
	public void teardown(){
		driver.quit();
	}
	@Test
	public void firstTestcase() {
		Exception ex = null;  
		   AssertionError Aer = null;
			try {
			Actions actions = new Actions(driver);
			WebElement mainMenu = driver.findElement(By.xpath("//*[contains(text(),'Services')]"));
			actions.moveToElement(mainMenu).moveToElement(driver.findElement(By.xpath("//*[contains(text(),'Automation')]"))).click().build().perform();
			String URl= "https://www.sogeti.com/services/automation/";
			String CurrentURL= driver.getCurrentUrl();
			AssertJUnit.assertEquals(URl, CurrentURL);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			mainMenu = driver.findElement(By.xpath("//nav[@class='main-menu-desktop']//ul[@class='level0 clearfix']//li[@class='selected has-children  expanded level2']"));
			AssertJUnit.assertNotNull(driver.findElement(By.xpath("//nav[@class='main-menu-desktop']//ul[@class='level0 clearfix']//li[@class='selected has-children  expanded level2']")));
			actions.moveToElement(mainMenu).moveToElement(driver.findElement(By.xpath("//div[@class='mega-navbar refreshed level2']//ul[@class='level1']//li[@class='selected  current expanded']")));
		    AssertJUnit.assertNotNull(driver.findElement(By.xpath("//div[@class='mega-navbar refreshed level2']//ul[@class='level1']//li[@class='selected  current expanded']")));					
			}catch(AssertionError e)
		    {
				Aer = e;
		        System.out.println("Assertion error. ");
		        
		    }
			catch(Exception  e)
		    {
				ex = e;
		        System.out.println("Element not found error. ");
		        
		    }
		     if(ex == null && Aer == null )
		    System.out.println("Testcase Succesfully Executed.");
		     else
		    System.out.println("Testcase Executed with errors"); 
		   //  Driver.close();
		    // Driver.quit();
			
		 	driver.quit();
				
}
	@Test
	public static void secondTestcase() {
		Exception ex = null;  
		   AssertionError Aer = null;
		   System.setProperty("webdriver.chrome.driver", "C:/Workspace/chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			driver.get("https://www.sogeti.com/");
			driver.manage().window().maximize();
			Actions actions = new Actions(driver);
			WebElement mainMenu = driver.findElement(By.xpath("//*[contains(text(),'Services')]"));
			actions.moveToElement(mainMenu).moveToElement(driver.findElement(By.xpath("//*[contains(text(),'Automation')]"))).click().build().perform();
			String URl= "https://www.sogeti.com/services/automation/";
			String CurrentURL= driver.getCurrentUrl();
			AssertJUnit.assertEquals(URl, CurrentURL);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			WebElement FName    = driver.findElement(By.id("4ff2ed4d-4861-4914-86eb-87dfa65876d8")); 
			WebElement LName    = driver.findElement(By.id("11ce8b49-5298-491a-aebe-d0900d6f49a7")); 
			WebElement Email    = driver.findElement(By.id("056d8435-4d06-44f3-896a-d7b0bf4d37b2"));
			WebElement Phone    = driver.findElement(By.id("755aa064-7be2-432b-b8a2-805b5f4f9384")); 
			WebElement Message  = driver.findElement(By.id("88459d00-b812-459a-99e4-5dc6eff2aa19"));
			WebElement Checkbox = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div/div[3]/div/div[3]/div[2]/div[6]/div[3]/form/div[2]/section/div[7]/div/div/label/input"));
			WebElement Button   = driver.findElement(By.id("06838eea-8980-4305-83d0-42236fb4d528"));
			try{
			FName.sendKeys(RandomStringUtils.randomAlphabetic(7));
			LName.sendKeys(RandomStringUtils.randomAlphabetic(7));
			Email.sendKeys(RandomStringUtils.randomAlphabetic(10)+"@sogeti.com");
			Phone.sendKeys(RandomStringUtils.randomNumeric(10));
			Message.sendKeys(RandomStringUtils.randomAlphabetic(25));
			Checkbox.click();
			Button.click();
			 
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			String ExpectedMsg = "Thank you for contacting us.";
			String ReceivedMsg = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div/div[3]/div/div[3]/div[2]/div[6]/div[3]/form/div[1]/div/p")).getText().toString();
			AssertJUnit.assertEquals(ExpectedMsg, ReceivedMsg);
			
			
			} catch(AssertionError e)
	        {
				Aer = e;
	            System.out.println("Assertion error. ");        
	        }
			catch(Exception  e)
	        {
				ex = e;
	            System.out.println("Element not found error. ");
	            
	        }
	         if(ex == null && Aer == null )
	        System.out.println("Testcase Succesfully Executed.");
	         else
	        System.out.println("Testcase Executed with errors"); 
			driver.close();
			driver.quit();
	       }
	@Test		
	public static void thirdTestcase() {
			 int er = 0;
			 String url = "";
			  String homePage = "https://www.sogeti.com/";
			  HttpURLConnection httpurlcon = null;
			  int respCode = 200;
			System.setProperty("webdriver.chrome.driver", "C:/Workspace/chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			driver.get(homePage);
			driver.manage().window().maximize();
			WebElement Worldwide = driver.findElement(By.xpath("//*[contains(text(),'Worldwide')]"));
			Worldwide.click();
			List <WebElement> CountryList = driver.findElements(By.xpath("//div[@class='country-list']//ul//li//a"));
			Iterator<WebElement> clit = CountryList.iterator();
			
			while(clit.hasNext()){
	            
	            url = clit.next().getAttribute("href");
	            
	            System.out.println(url);
	        
	            if(url == null || url.isEmpty()){
	System.out.println("URL is either not configured for anchor tag or it is empty");
	                continue;
	            }    
	            try {
	            	httpurlcon = (HttpURLConnection)(new URL(url).openConnection());
	                
	            	httpurlcon.setRequestMethod("HEAD");
	                
	            	httpurlcon.connect();
	                
	                respCode = httpurlcon.getResponseCode();
	                
	                if(respCode >= 400){
	                    System.out.println(url+" is a broken link");
	                    er++;
	                }
	                else{
	                	
	                    System.out.println(url+" is a valid link");
	                }
	                    
	            } catch (MalformedURLException e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	                er++;
	            } catch (IOException e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	                er++;
	            }
	        }
			if(er == 0 )
		        System.out.println("Testcase Succesfully Executed.");
		         else
		        System.out.println("Testcase Executed with errors"); 
	        driver.quit();
		
			
			
	}	
}	

